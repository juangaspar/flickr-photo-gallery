import request from 'supertest';
import server from '../../server/index';

describe('Node server tests', function () {
  it('responds to /', done => {
    request(server)
      .get('/')
      .expect(200, done);
  });

  it('404 anything else', done => {
    request(server)
      .get('/anything')
      .expect(404, done);
  });

});