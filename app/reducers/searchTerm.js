import { UPDATE_SEARCH_TERM } from '../actions/index';


export default function (searchTerm = '', action) {
  switch (action.type) {
    case UPDATE_SEARCH_TERM:
      return action.payload;
  }
  return searchTerm;
}