import PhotosListReducer from './photosList';
import SearchTermReducer from './searchTerm';
import SelectedPhoto from './selectedPhoto';

export default {
  searchTerm: SearchTermReducer,
  photosList: PhotosListReducer,
  selectedPhoto: SelectedPhoto
};
