import { UPDATE_SELECTED_PHOTO } from '../actions/index';


export default function (selectedPhoto = null, action) {
  switch (action.type) {
    case UPDATE_SELECTED_PHOTO:
      return action.payload;
  }
  return selectedPhoto;
}