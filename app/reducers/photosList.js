import { FETCH_PHOTOS, FETCH_MORE_PHOTOS, CLEAN_PHOTOS } from '../actions/index';

const photosInitialState = { photos: { page: 1, pages: 0, total: '0', photo: [], perpage: 10 }, stat: null };
const photosCleanState = { ...photosInitialState, stat: 'clean' };
const photosErrorState = { ...photosInitialState, stat: 'error' };

export default function (photosList = photosInitialState, action) {
  switch (action.type) {
    case FETCH_PHOTOS:
      if (!action.payload.data)
        return photosErrorState;
      else
        return action.payload.data;
    case FETCH_MORE_PHOTOS:
      return { ...action.payload.data, photos: { ...action.payload.data.photos, photo: [...photosList.photos.photo, ...action.payload.data.photos.photo] } };
    case CLEAN_PHOTOS:
      return photosCleanState;
  }
  return photosList;
}