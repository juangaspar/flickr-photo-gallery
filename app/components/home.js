import React, { Component } from 'react';
import SearchBar from '../containers/SearchBar';

/**
 * App component
 */
export default class Home extends Component {
  render() {
    return (
      <div className="home">
        <center>
          <span className="title">Flicker Photo Gallery</span>
          <SearchBar />
        </center>
      </div>
    );
  }
}