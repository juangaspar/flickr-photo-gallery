import React, { Component } from 'react';
import SearchBar from '../containers/SearchBar';
import PhotosGrid from '../containers/PhotosGrid';
import PhotoViewer from '../containers/PhotoViewer';

/**
 * App component
 */
export default class Photos extends Component {
    render() {
        return (
            <div className="photos"><SearchBar /><PhotosGrid /><PhotoViewer /></div>
        );
    }
}
