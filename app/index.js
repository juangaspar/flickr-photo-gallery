import 'babel-polyfill';
import React from 'react';
import ReactDOM from 'react-dom';
import { Provider } from 'react-redux';
import { createStore, combineReducers, applyMiddleware } from 'redux';
import promise from 'redux-promise';
import createHistory from 'history/createBrowserHistory';
import { Route } from 'react-router';
import { ConnectedRouter, routerReducer, routerMiddleware, push } from 'react-router-redux';

// Add offline runtime and install it
import * as OfflinePluginRuntime from 'offline-plugin/runtime';
OfflinePluginRuntime.install();

import Home from './components/home';
import Photos from './components/photos';
import reducers from './reducers';

// Create a history of your choosing (we're using a browser history in this case)
const history = createHistory();

// Build the middleware for intercepting and dispatching navigation actions
const middleware = routerMiddleware(history);

let store;

if (window.__PRELOADED_STATE__) {
  const preloadedState = window.__PRELOADED_STATE__;

  // Allow the passed state to be garbage-collected
  delete window.__PRELOADED_STATE__;

  store = createStore(
    combineReducers({
      ...reducers,
      router: routerReducer
    }),
    preloadedState,
    applyMiddleware(promise, middleware)
  );
}
else {
  store = createStore(
    combineReducers({
      ...reducers,
      router: routerReducer
    }),
    applyMiddleware(promise, middleware)
  );
}

import './styles/main.scss';

ReactDOM.render(
  <Provider store={store}>
    <ConnectedRouter history={history}>
      <div className="routes-container">
        <Route exact path="/" component={Home} />
        <Route path="/photos" component={Photos} />
      </div>
    </ConnectedRouter>
  </Provider>
  , document.querySelector('.container'));
