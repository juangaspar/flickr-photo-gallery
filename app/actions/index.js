import axios from 'axios';

/** Actions types */
export const FETCH_PHOTOS = 'FETCH_PHOTOS';
export const FETCH_MORE_PHOTOS = 'FETCH_MORE_PHOTOS';
export const CLEAN_PHOTOS = 'CLEAN_PHOTOS';
export const UPDATE_SEARCH_TERM = 'UPDATE_SEARCH_TERM';
export const UPDATE_SELECTED_PHOTO = 'UPDATE_SELECTED_PHOTO';

const API_URL = (typeof window !== 'undefined')?window.__SERVER_URL__:process.env.__SERVER_URL__;


/**
 * Fetch for photos filtering by the given parameters
 *
 * @param {String} searchTerm
 * @param {Number} page
 * @param {Number} perPage
 * @return {Object}
 */
export function fetchPhotos(searchTerm, page, perPage) {
  const encodedTerm = encodeURI(searchTerm);
  const request = axios.get(`${API_URL}/photos/${encodedTerm}/${page}/${perPage}`);

  return {
    type: FETCH_PHOTOS,
    payload: request
  };
}


/**
 * Fetch for more photos filtering by the given parameters
 *
 * @param {String} searchTerm
 * @param {Number} page
 * @param {Number} perPage
 * @return {Object}
 */
export function fetchMorePhotos(searchTerm, page, perPage) {
  const encodedTerm = encodeURI(searchTerm);
  const request = axios.get(`${API_URL}/photos/${encodedTerm}/${page}/${perPage}`);

  return {
    type: FETCH_MORE_PHOTOS,
    payload: request
  };
}


/**
 * Clean photos list
 *
 * @return {Object}
 */
export function cleanPhotos() {
  return {
    type: CLEAN_PHOTOS,
    payload: null
  };
}


/**
 * Updated search term
 *
 * @param {String} searchTerm
 * @return {Object}
 */
export function updateSearchTerm(searchTerm) {
  return {
    type: UPDATE_SEARCH_TERM,
    payload: searchTerm
  };
}


/**
 * Updated selected photo
 *
 * @param {Object} photo
 * @return {Object}
 */
export function updateSelectedPhoto(photo) {
  return {
    type: UPDATE_SELECTED_PHOTO,
    payload: photo
  };
}