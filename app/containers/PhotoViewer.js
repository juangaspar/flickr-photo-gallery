import React, { Component } from 'react';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import Moment from 'moment';

import { updateSelectedPhoto } from '../actions/index';

/**
 * Photo viewer component
 */
class PhotoViewer extends Component {

  /**
   * Component constructor
   * @param {Object} props
   */
  constructor(props) {
    super(props);
  }


  /**
   * Render component method
   *
   * @return {Object}
   */
  render() {
    if (!this.props.selectedPhoto) {
      document.querySelector('.container').classList.remove('block-scroll');
      return null;
    }

    document.querySelector('.container').classList.add('block-scroll');

    // Prepare photo info data
    const viewerUrl = this.props.selectedPhoto.url_l || this.props.selectedPhoto.url_n;
    const dateTaken = (this.props.selectedPhoto.datetakenunknown == '1') ? '--' : Moment(this.props.selectedPhoto.datetaken).fromNow();
    const dateUploaded = Moment(this.props.selectedPhoto.dateupload * 1000).fromNow();

    return (
      <div className="photo-viewer">
        <center className="photo-viewer-info"><div>
          <b>User</b> {this.props.selectedPhoto.ownername} <b>Uploaded</b> {dateUploaded} <b>Taken</b> {dateTaken}
        </div></center>
        <button onClick={() => this.props.updateSelectedPhoto(null)} className="photo-viewer-close-button"><i className="fa fa-close fa-2x"></i></button>
        <div className="photo-viewer-img"
          style={{ background: `transparent url(${viewerUrl}) center / contain no-repeat` }}></div>
      </div>
    );
  }
}

function mapDispatchToProps(dispatch) {
  return bindActionCreators({ updateSelectedPhoto }, dispatch);
}


function mapStateToProps({ selectedPhoto }) {
  return { selectedPhoto };
}

export default connect(mapStateToProps, mapDispatchToProps)(PhotoViewer);