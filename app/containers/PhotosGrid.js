import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import { fetchMorePhotos, updateSelectedPhoto } from '../actions/index';
import InfiniteScroll from 'react-infinite-scroll-component';

/**
 * Photos grid component
 */
class PhotosGrid extends Component {

  /**
   * Component constructor
   * @param {Object} props
   */
  constructor(props) {
    super(props);
  }


  /**
   * Generate photo html
   *
   * @param {Object} photo
   * @return {Object}
   */
  renderPhoto(photo) {
    const key = `${photo.id}${Math.random() * 10000}`;
    const photoImageSrc = photo.url_n || photo.url_o;

    return (
      <div key={key} className="photo-grid-box">
        <div className="photo-grid-content" onClick={() => { this.viewPhoto(photo); }}>
          <i className="fa fa-circle-o-notch fa-spin fa-2x fa-fw"></i>
          <img src={photoImageSrc} />
          <div className="photo-grid-info">
            <button onClick={(e) => { e.stopPropagation(); this.openPhotoPost(photo); }} className='photo-grid-user'>
              <i className="fa fa-flickr"></i>&nbsp;&nbsp;{photo.ownername}
            </button>
            <span>{photo.title}</span>
          </div>
        </div>
      </div>
    );
  }


  /**
   * Navigate to photo viewer
   *
   * @param {Object} photo
   */
  viewPhoto(photo) {
    this.props.updateSelectedPhoto(photo);
  }


  /**
   * Open photo post
   *
   * @param {Object} photo
   */
  openPhotoPost(photo) {
    window.open(`https://www.flickr.com/photos/${photo.owner}/${photo.id}`);
  }


  /**
   * Check if more photos can be loaded
   *
   * @return {boolean}
   */
  checkPhotosLoadMore() {
    return ((parseInt(this.props.photosList.photos.total) / this.props.photosList.photos.perpage) > this.props.photosList.photos.page);
  }


  /**
   * Call action to load more photos
   */
  loadMorePhotos() {
    this.props.fetchMorePhotos(this.props.searchTerm, this.props.photosList.photos.page + 1, 20)
  }


  /**
   * Return info message content
   *
   * @param {String} infoMessage
   * @return {Object}
   */
  getInfoRender(infoMessage) {
    return <div className="photos-grid-info-message">{infoMessage}</div>;
  }


  /**
   * Return photos grid element
   *
   * @return {Object}
   */
  getPhotosGridRender() {
    // Check load more property
    var loadMore = this.checkPhotosLoadMore();

    return (
      <InfiniteScroll
        next={() => { this.loadMorePhotos() }}
        hasMore={loadMore}
        endMessage={<div className="photos-grid-info-message">No more photos</div>}
        loader={<div className="photos-grid-info-message">Loading</div>}>
        <div className="photos-grid">
          {this.props.photosList.photos.photo.map((photo) => this.renderPhoto(photo))}
        </div>
      </InfiniteScroll>
    );
  }


  /**
   * Components render method
   *
   * @return {Object}
   */
  render() {
    let renderedContent;

    // Check photos list property to render the right info
    if (this.props.photosList.stat == null) {
      renderedContent = this.getInfoRender('Search images from Flicker');
    }
    else if (this.props.photosList.stat == 'clean') {
      renderedContent = this.getInfoRender('Searching...');
    }
    else if (this.props.photosList.stat == 'error') {
      renderedContent = this.getInfoRender('Cannot retrieve photos');
    }
    else if (this.props.photosList.photos.photo.length == 0) {
      renderedContent = this.getInfoRender('No photos');
    }
    else {
      renderedContent = this.getPhotosGridRender();
    }

    return renderedContent;
  }
}

PhotosGrid.contextTypes = {
  router: PropTypes.object
};


function mapDispatchToProps(dispatch) {
  return bindActionCreators({ fetchMorePhotos, updateSelectedPhoto }, dispatch);
}


function mapStateToProps({ photosList, searchTerm }) {
  return { photosList, searchTerm };
}

export default connect(mapStateToProps, mapDispatchToProps)(PhotosGrid);
