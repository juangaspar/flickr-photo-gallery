import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import { fetchPhotos, cleanPhotos, updateSearchTerm } from '../actions/index';

/**
 * Search bar component
 */
class SearchBar extends Component {

  /**
   * Component constructor
   * @param {Object} props
   */
  constructor(props) {
    super(props);
  }


  /**
   * Update search term value every search bar value changes
   *
   * @param {Event} event
   */
  onInputChange(event) {
    this.props.updateSearchTerm(event.target.value);
  }


  /**
   * Fetch photos when user press Enter key in search bar
   *
   * @param {Event} event
   */
  onKeyUp(event) {
    if (event.keyCode == 13 && this.props.searchTerm) {
      this.context.router.history.replace('/photos')
      this.props.cleanPhotos();
      this.props.fetchPhotos(this.props.searchTerm, 1, 20);
    }
  }


  /**
   * Request for photos on seearch button click
   */
  onSearchButtonClick() {
    this.context.router.history.replace('/photos')
    this.props.cleanPhotos();
    this.props.fetchPhotos(this.props.searchTerm, 1, 20);
  }


  /**
   * Render component method
   *
   * @return {Object}
   */
  render() {
    return (
      <div className="search-bar">
        <input
          placeholder=""
          className=""
          value={this.props.searchTerm}
          onChange={event => this.onInputChange(event)}
          onKeyUp={event => this.onKeyUp(event)} />
        <button onClick={() => this.onSearchButtonClick()} className="btn btn-secondary" disabled={!this.props.searchTerm}><i className="fa fa-search"></i></button>
      </div>
    );
  }
}

SearchBar.contextTypes = {
  router: PropTypes.object
};

function mapDispatchToProps(dispatch) {
  return bindActionCreators({ fetchPhotos, cleanPhotos, updateSearchTerm }, dispatch);
}


function mapStateToProps({ searchTerm }) {
  return { searchTerm };
}

export default connect(mapStateToProps, mapDispatchToProps)(SearchBar);