import React from 'react';
import { renderToString } from 'react-dom/server';
import { Provider } from 'react-redux';
import { createStore, combineReducers, applyMiddleware } from 'redux';
import promise from 'redux-promise';
import createHistory from 'history/createMemoryHistory';
import { Route } from 'react-router';
import { ConnectedRouter, routerReducer, routerMiddleware, push } from 'react-router-redux';

import Home from '../app/components/home';
import Photos from '../app/components/photos';
import reducers from '../app/reducers';

// Create a history of your choosing (we're using a browser history in this case)
const history = createHistory();

// Build the middleware for intercepting and dispatching navigation actions
const middleware = routerMiddleware(history);

export default function handleRender(req, res) {
  // Create a new Redux store instance
  const store = createStore(
    combineReducers({
      ...reducers,
      router: routerReducer
    }),
    applyMiddleware(promise, middleware)
  );

  // Render the component to a string
  const html = renderToString(
    <Provider store={store}>
      <ConnectedRouter history={history}>
        <div className="routes-container">
          <Route exact path="/" component={Home} />
          <Route path="/photos" component={Photos} />
        </div>
      </ConnectedRouter>
    </Provider>
  )

  // Grab the initial state from our Redux store
  const preloadedState = store.getState();

  // Send the rendered page back to the client
  res.send(renderFullPage(html, preloadedState))
}


function renderFullPage(html, preloadedState) {

  const serverUrl = `${process.env.SERVER_PROTOCOL}://${process.env.SERVER_URL}:${process.env.SERVER_PORT}`;

  return `
    <!DOCTYPE html>
    <html>

    <head>
      <title>Flickr photo gallery</title>

      <link rel="apple-touch-icon" sizes="180x180" href="/assets/img/apple-touch-icon.png">
      <link rel="icon" type="image/png" href="/assets/img/favicon-32x32.png" sizes="32x32">
      <link rel="icon" type="image/png" href="/assets/img/favicon-16x16.png" sizes="16x16">
      <link rel="manifest" href="/assets/manifest.json">
      <link rel="mask-icon" href="/assets/img/safari-pinned-tab.svg" color="#5bbad5">
      <link rel="shortcut icon" href="/assets/img/favicon.ico">
      <meta name="msapplication-config" content="/assets/browserconfig.xml">
      <meta name="theme-color" content="#0892d0">

      <meta name="viewport" content="width=device-width, initial-scale=1.0">
      <link href="/app.css?7f78a19fa8bfda826a81" rel="stylesheet"/></head>

    <body>
      <div class="container">${html}</div>
      <script>
          window.__PRELOADED_STATE__ = ${JSON.stringify(preloadedState).replace(/</g, '\\u003c')}
          window.__SERVER_URL__ = '${serverUrl}';
        </script>
        <script type="text/javascript" src="/app.js?7f78a19fa8bfda826a81"></script></body>

    </html>
    `
}