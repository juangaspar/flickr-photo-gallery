import dotenv from 'dotenv';
import path from 'path';
import express from 'express';
import handleRender from './render';
import bodyParser from 'body-parser';
import cors from 'cors';
import http from 'http';
import http2 from 'spdy';
import fs from 'fs';

// Configure environment properties
const envPath = (process.env.NODE_ENV == 'production') ? 'config/env.production' : 'config/env.dev';
dotenv.config({
  path: envPath
});

process.env.__SERVER_URL__ = `${process.env.SERVER_PROTOCOL}://${process.env.SERVER_URL}:${process.env.SERVER_PORT}`;

//Configure app
const app = express();
app.use(cors());
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({
  extended: true
}));


//Serve static files
app.use(express.static('dist'));

// Configure routes
import routes from './routes/index'
routes(app);

// Create server and start listening
let server;

if (process.env.SERVER_PROTOCOL == 'https') {
  const options = {
    key: fs.readFileSync(process.env.SERVER_HTTPS_KEY_PATH),
    cert: fs.readFileSync(process.env.SERVER_HTTPS_CERT_PATH)
  }

  server = http2.createServer(options, app);
}
else {
  server = http.createServer(app);
}

server.listen(process.env.SERVER_PORT, function () {
  console.log(`Listening on: ${process.env.__SERVER_URL__}`);
});

export default server;