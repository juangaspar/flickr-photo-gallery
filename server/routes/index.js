import Flickr from 'flickrapi';
import handleRender from '../render';

export default function (app) {
  const getPhotos = (req, res) => {
    const flickrOptions = {
      api_key: process.env.FLICKR_API_TOKEN,
      secret: process.env.FLICKR_API_SECRET
    };

    Flickr.tokenOnly(flickrOptions, (error, flickr) => {
      flickr.photos.search({
        text: req.params.search_term,
        page: req.params.page,
        per_page: req.params.per_page,
        extras: 'owner_name,url_o,url_n,url_l,date_upload,date_taken'
      }, (err, result) => {
        if (err) {
          throw new Error(err);
        }
        res.send(result);
      });
    });
  }

  app.get('/', handleRender);
  app.get('/photos', (req, res) => {
    res.redirect('/');
  });
  app.get('/photos/:search_term/:page/:per_page', getPhotos);
};