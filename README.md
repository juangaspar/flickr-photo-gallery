# Flickr photo gallery

This is a demo app built with React framework

### Getting Started

To setup and run it, first clone the repo and then install dependencies 

```
> git clone https://juangaspar@bitbucket.org/juangaspar/flickr-photo-gallery.git
> cd flickr-photo-gallery
```

Server configuration is in config/env.* files. Please set needed parameters before start the server process.

```
FLICKR_API_TOKEN=
FLICKR_API_SECRET=
SERVER_PROTOCOL=http
SERVER_URL=localhost
SERVER_PORT=8080
SERVER_HTTPS_KEY_PATH=
SERVER_HTTPS_CERT_PATH=
```

If you use Yarn (https://yarnpkg.com/en/docs/install)

```
> yarn install
> yarn build
> yarn serve
```

If you are using NPM

```
> npm run install
> npm run build
> npm run serve
```

To run the tests

```
> yarn test \\npm run test
```
