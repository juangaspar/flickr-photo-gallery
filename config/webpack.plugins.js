// Import plugins
import ExtractTextPlugin from 'extract-text-webpack-plugin';
import CopyPlugin from 'copy-webpack-plugin';
import OfflinePlugin from 'offline-plugin';

const extractText = new ExtractTextPlugin({
  filename: "app.css"
});

const copy = new CopyPlugin(
  [{
    from: 'assets',
    to: 'assets'
  }], {}
);

const offline = new OfflinePlugin({
  caches: {
    main: [
      '/'
    ],
    additional: [
      ':rest:'
    ]
  },
  responseStrategy: 'network-first',
  ServiceWorker: {
    navigateFallback: '/'
  }
});

export default {
  extractText,
  copy,
  offline
};