import plugins from './config/webpack.plugins';

export default {
  entry: [
    'babel-polyfill',
    './app/index.js'
  ],
  output: {
    path: __dirname + '/dist',
    publicPath: '/',
    filename: 'app.js'
  },
  module: {
    rules: [{
      exclude: /node_modules/,
      loader: 'babel-loader',
      options: {
        presets: ['react', 'es2015', 'stage-1']
      }
    }, {
      test: /\.scss$/,
      use: plugins.extractText.extract({
        use: [{
          loader: 'css-loader',
          options: {
            importLoaders: 1
          }
        }, {
          loader: 'postcss-loader',
          options: {
            plugins: function () {
              return [
                require('autoprefixer')
              ];
            }
          }
        }, {
          loader: 'sass-loader'
        }]
      })
    }]
  },
  resolve: {
    extensions: ['.js', '.jsx']
  },
  plugins: [plugins.extractText, plugins.copy, plugins.offline]
};